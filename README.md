# Dating web application.

This application contains two parts:
1. Web client: Angular 8.
2. Web API or microservices: ASP.NET Core.

## Prerequisites:
* NodeJS v10.16.2: https://nodejs.org/en/
* Angular CLI v8.2.2 (run this command: `npm install -g @angular/cli`)
* Dotnet Core SDK v2.2  (https://dotnet.microsoft.com/download)

## Installation:
* Move to folder DatingApp-Spa and run this command: `npm install`
* Go to file ngx-gallery.umd.js in node_modules folder and change this:

```
var CustomHammerConfig = /** @Class */ (function (_super) {
__extends(CustomHammerConfig, _super);
function CustomHammerConfig() {
var _this = _super !== null && _super.apply(this, arguments) || this;
_this.overrides = ({
'pinch': { enable: false },
'rotate': { enable: false }
});
return _this;
}
return CustomHammerConfig;
}(platformBrowser.HammerGestureConfig));
```

to :

```
class CustomHammerConfig extends platformBrowser.HammerGestureConfig {
    constructor() {
        super(...arguments);
        this.overrides = ({
            'pinch': { enable: false },
            'rotate': { enable: false }
        });
    }
}
```
Go to DatingApp.API folder run this command to install necessary libraries for asp.net: `dotnet restore`
## Run the application
* Go to DatingApp-SPA and run this command: `ng serve`. This's gonna host the web client on this address: localhost:4200
* Go to DatingApp.API and run this command: `dotnet run`. This's gonna host the web API on this address: localhost:5000
* Access localhost:4200 on your web browser.
* Username: candy
* Password: password
